import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import Swal from 'sweetalert2';
import { Validators } from '@angular/forms';
// Servicio
import { AnimalesService } from 'src/app/service/animales.service';
// Componente
import { modelAnimales } from '../../../../Models/modelAnimales';
import { Router } from '@angular/router';



@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css'],
})
export class FormularioComponent implements OnInit {
  public Animal: any[] = [];
  AnimalF: FormGroup;

  constructor(
    private _AnimalesService: AnimalesService,
    private fb: FormBuilder,
    private router: Router,
  ) {
    this.AnimalF = this.fb.group({
      nombreAnimal: ['', Validators.required],
      especie: ['', Validators.required],
      edad: ['', Validators.required],
      nombreDuenio: ['', Validators.required],
      apellidosDuenio: ['', Validators.required],
      telefonoDuenio: ['', Validators.required],
      motivoConsulta: ['', Validators.required],
      fechaConsulta: ['', Validators.required],
      fechaSalidaConsulta: ['', Validators.required],
      costoConsulta: ['', Validators.required],
      diagnostico: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.BuscarAnimales();
  }
  BuscarAnimales() {
    this.Animal = [];
    this._AnimalesService.getAnimales().subscribe(
      (data: any) => {
        this.Animal = data.data;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  postAnimal() {
    console.log(this.Animal);

    // Recoge los datos del formulario
    const ANIMAL: modelAnimales = {
      nombreAnimal: this.AnimalF.get('nombreAnimal')?.value,
      especie: this.AnimalF.get('especie')?.value,
      edad: this.AnimalF.get('edad')?.value,
      nombreDuenio: this.AnimalF.get('nombreDuenio')?.value,
      apellidosDuenio: this.AnimalF.get('apellidosDuenio')?.value,
      telefonoDuenio: this.AnimalF.get('telefonoDuenio')?.value,
      motivoConsulta: this.AnimalF.get('motivoConsulta')?.value,
      fechaConsulta: this.AnimalF.get('fechaConsulta')?.value,
      fechaSalidaConsulta: this.AnimalF.get('fechaSalidaConsulta')?.value,
      costoConsulta: this.AnimalF.get('costoConsulta')?.value,
      diagnostico: this.AnimalF.get('diagnostico')?.value,
    };

    this._AnimalesService.postAnimal(ANIMAL).subscribe(
      (data) => {
        this.Exito();
        this.BuscarAnimales();
        this.router.navigate(['/animales']);
      },
      (error) => {
        console.log(error);
        this.AnimalF.reset();
      }
    );
  }

  // Alertas
  Exito() {
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: 'Registrado con Exito',
      showConfirmButton: false,
      timer: 1500,
    });
  }

  Actualizado() {
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: 'Actualizado con Exito',
      showConfirmButton: false,
      timer: 1500,
    });
  }
}
