export class modelAnimales {
    _id?: Object;
    nombreAnimal: String;
    especie: String;
    edad: Number;
    nombreDuenio: String;
    apellidosDuenio: String;
    telefonoDuenio: Number;
    motivoConsulta: String;
    fechaConsulta: Date;
    fechaSalidaConsulta: Date;
    costoConsulta: Number;
    diagnostico: String;

    constructor(nombreAnimal: String, especie: String, edad: Number, nombreDuenio: String, apellidosDuenio: String, telefonoDuenio: Number, motivoConsulta: String, fechaConsulta: Date, fechaSalidaConsulta: Date, costoConsulta: Number, diagnostico: String,){
        this.nombreAnimal = nombreAnimal;
        this.especie = especie;
        this.edad = edad;
        this.nombreDuenio = nombreDuenio;
        this.apellidosDuenio = apellidosDuenio;
        this.telefonoDuenio = telefonoDuenio;
        this.motivoConsulta = motivoConsulta;
        this.fechaConsulta = fechaConsulta;
        this.fechaSalidaConsulta = fechaSalidaConsulta;
        this.costoConsulta = costoConsulta;
        this.diagnostico = diagnostico;
    }
}