const express = require('express');
const nuevoAnimal = require('../../Models/animales/modelAnimales');


let app = express();

//nuevo dato inicio de metodo post
app.post('/animal/nuevo', (req, res) => {
    let body = req.body;
    console.log(body);

    let newSchemaAnimal = new nuevoAnimal({

        nombreAnimal: body.nombreAnimal,
        especie: body.especie,
        edad: body.edad,
        nombreDuenio: body.nombreDuenio,
        apellidosDuenio: body.apellidosDuenio,
        telefonoDuenio: body.telefonoDuenio,
        motivoConsulta: body.motivoConsulta,
        fechaConsulta: body.fechaConsulta,
        fechaSalidaConsulta: body.fechaSalidaConsulta,
        costoConsulta: body.costoConsulta,
        diagnostico: body.diagnostico
    })
    newSchemaAnimal
    .save()
    .then(
        (data) => {
            return res.status(200)
            .json({
                ok: true,
                message: 'Datos guardados',
                data
            });
        }
        
    )
    .catch(
        (err) => {
            return res.status(500)
            .json({
                ok: false,
                message: 'Ocurrio un problema',
                err
            })
        }
    )
});
//inicio metodo get

app.get('/todos/animales', async(req, res) =>{
    await nuevoAnimal
        .find()
        .then(
            (data) => {
                console.log(data);
                return res.status(200)
                    .json({
                        ok: true,
                        message: 'Datos Encontrados con éxito',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message: 'Vayaaaa... ha ocurrido un error',
                        err
                    });
            }
        );
})

//Fin metodo get

//Inicia metodo get by id
app.get('/id/animales/:id', async(req, res) =>{
    await nuevoAnimal
        .findById(req.params.id)
        .then(
            (data) => {
                console.log(data);
                return res.status(200)
                    .json({
                        ok: true,
                        message: 'Datos Encontrados con éxito',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message: 'Vayaaaa... ha ocurrido un error',
                        err
                    });
            }
        );
})

//fin del metodo get by id

//inicio metodo put
app.put('/cambiar/animal/:id', async(req, res) =>{
    const{
        nombreAnimal,
        especie,
        edad,
        nombreDuenio,
        apellidosDuenio,
        telefonoDuenio,
        motivoConsulta,
        fechaConsulta,
        fechaSalidaConsulta,
        costoConsulta,
        diagnostico
        } = req.body;

    let animalNuevo = await nuevoAnimal

        .findById(req.params.id)
        
        animalNuevo.nombreAnimal = nombreAnimal;
        animalNuevo.especie = especie;
        animalNuevo.edad = edad;
        animalNuevo.nombreDuenio = nombreDuenio;
        animalNuevo.apellidosDuenio = apellidosDuenio;
        animalNuevo.telefonoDuenio = telefonoDuenio;
        animalNuevo.motivoConsulta = motivoConsulta;
        animalNuevo.fechaConsulta = fechaConsulta;
        animalNuevo.fechaSalidaConsulta = fechaSalidaConsulta;
        animalNuevo.costoConsulta = costoConsulta;
        animalNuevo.diagnostico = diagnostico;



        animalNuevo = await nuevoAnimal.findOneAndUpdate({_id: req.params.id},animalNuevo,{new:true})

        .then(
            (data) => {
                console.log(data);
                return res.status(200)
                    .json({
                        ok: true,
                        message: 'Datos actializados',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message: 'Vayaaaa... ha ocurrido un error',
                        err
                    });
            }
        );
})

//fin metodo put
//inicio metodo delete
app.delete('/eliminar/animal/:id', async(req, res) =>{
    let animalNuevo = await nuevoAnimal

        .findById(req.params.id)

        await nuevoAnimal.findOneAndRemove({_id: req.params.id})
        .then(
            (data) => {
                console.log(data);
                return res.status(200)
                    .json({
                        ok: true,
                        message: 'Datos eliminados',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message: 'Vayaaaa... ha ocurrido un error',
                        err
                    });
            }
        );
})

//fin metodo delete

module.exports = app;