import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { modelAnimales } from '../../../Models/modelAnimales';
@Injectable({
  providedIn: 'root',
})
export class AnimalesService {
  localh = 'http://localhost:3900';
  urlGet = this.localh + '/todos/animales';
  urlPut = this.localh + '/cambiar/animal/';
  urlPost = this.localh + '/animal/nuevo';
  urlGetById = this.localh + '/id/animales/';
  urlDelete = this.localh + '/eliminar/animal/';

  constructor(private http: HttpClient) {}

  getAnimales():Observable <any>{
    return this.http.get(this.urlGet);
  }
  getByIdAnimal(id: String): Observable <any> {
    return this.http.get(this.urlGetById + id);
  }
  postAnimal(Animal: modelAnimales): Observable <any> {
    return this.http.post(this.urlPost, Animal);
  }
  putAnimal(id: String, Animal: modelAnimales): Observable <any> {
    return this.http.put(this.urlPut+id, Animal);
  }
  deleteAnimal(id: String): Observable <any> {
    return this.http.delete(this.urlDelete + id);
  }
}
